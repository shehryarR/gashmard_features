import numpy as np
import pandas as pd
import sklearn
from collections import Counter
    # from mendeleev import element

from mendeleev import element
from catboost import CatBoostRegressor
from catboost import CatBoost, Pool
import catboost
from sklearn.model_selection import train_test_split
import mendeleev
from mendeleev import element
from mendeleev import *
from mendeleev.fetch import fetch_ionization_energies
from matminer.featurizers.composition.element import ElementFraction

import pymatgen
from pymatgen.core.composition import Composition
from matminer.featurizers.conversions import StrToComposition
from matminer.featurizers.composition import ElementProperty
from matminer.featurizers.composition.element import ElementFraction
import chemparse

from pymatgen.core.composition import Composition
from pymatgen.core.periodic_table import get_el_sp, Element, Specie, DummySpecie
from pymatgen.util.string import formula_double_format


DataSets=[
    r'data/Spercon_Main_Dataset_20562sample.csv',
    r'data/DataG_13052sample.csv',
]


# def selectDataSet(selection):
    
# Selection=0
# TargetElementDataFrame=pd.read_csv(DataSets[Selection])


# x_Features=TargetElementDataFrame[TargetElementDataFrame.columns[DataSets[Selection][1]:DataSets[Selection][2]]]

# def readDataSets(DataSets):

Supercon_Main=pd.read_csv(DataSets[0])
# Supercon_Main
Supercon_13052=pd.read_csv(DataSets[1])
x_Features_47_103=Supercon_13052[Supercon_13052.columns[9:160]]


List_Features_47_103=list(x_Features_47_103)
y_13052=Supercon_13052.Tc_mean







# DataFrame_new

model_total = CatBoostRegressor(iterations=4000, learning_rate=0.1, depth=10, logging_level='Silent', random_state=42, 
                    early_stopping_rounds=200 ,l2_leaf_reg=3,thread_count=4,snapshot_interval=600, one_hot_max_size=30
                    , eval_metric='AUC', bagging_temperature=3, subsample=0.4,random_strength=0.5,od_pval=0.001,grow_policy='SymmetricTree',
                    min_child_samples=1, sampling_frequency='PerTree' )



model_total.fit(x_Features_47_103, y_13052)
model_total.save_model('./model_total')
list_element_Main_dataset=list(Supercon_Main.element)

                    
# Main

list_element_Main_dataset_Normalaize=list(Supercon_Main.Element_Normalize)
tekrar_elements= Counter(list_element_Main_dataset_Normalaize)

Main_List_Compound=[]
for index, row in DataFrame_new.iterrows():
    Element= row["element"]
    # print(Element)
    comp=Composition(Element)
    g=comp.reduced_formula
    Element_Normalize=comp.get_integer_formula_and_factor()
    Main_List_Compound.append(Element_Normalize[0])
DataFrame_new['Element_Normalize']=Main_List_Compound

for i in range(len(DataFrame_new)):
    new_compound=(DataFrame_new['element'][i])
    new_compound_Normalize=DataFrame_new['Element_Normalize'][i]
    # print(new_compound)
    index_2=DataFrame_new.index[i]
    list_element=[]
    list_Tc=[]
    Main_index=[] 

    if new_compound_Normalize not in list_element_Main_dataset_Normalaize:
        data=[new_compound]
        # print(type(data))
        TargetElementDataFrame=pd.TargetElementDataFrame()
        
        TargetElementDataFrame=pd.TargetElementDataFrame(data,columns=['element'])
        # print(TargetElementDataFrame)  
        print(new_compound,'is not exist in the main dataset.')
        print('Prediction of the transition temperature for',new_compound,'Using the model based on machine learning algorithm is=',end=" "),Prediction_Tc(TargetElementDataFrame), print("K"),print("\n")

    elif new_compound_Normalize in list_element_Main_dataset_Normalaize:
        for j in range(len(Supercon_Main)):
            element=Supercon_Main['element'][j]
            Tc_1=Supercon_Main['Tc'][j]
            element_Normalize=Supercon_Main['Element_Normalize'][j]    

            if  element_Normalize == new_compound_Normalize:

                if tekrar_elements[new_compound_Normalize] == 1:
                    print(new_compound,'exists in the main dataset and it is repeated for',tekrar_elements[new_compound_Normalize], 'times')
                    print('The transition temperature reported in the main dataset for',new_compound,'is =',Tc_1,'K'), print("\n")
                
                if tekrar_elements[new_compound_Normalize] > 1:
                    index_1=Supercon_Main.index[j]
                    Main_index.append(index_1)
                    if len(Main_index) == 1:
                        print(new_compound,'exists in the main dataset and it is repeated for',tekrar_elements[new_compound_Normalize], 'times')
                        print("The transition temperatures reported for",new_compound,"in the dataset are:"), print("\n")
                    
                    list_Tc.append(float(Tc_1))
                if tekrar_elements[new_compound_Normalize] == len(list_Tc):
                    for k in range(tekrar_elements[new_compound_Normalize]):
                        print(list_Tc[k],'K',",",end=" "), print("\n")

                    
def Prediction_Tc(TargetElementDataFrame):

    # global new_compound

# Number_of_Elements
    Number_of_Elements=[]
    for row in TargetElementDataFrame.iterrows():
        comp = Composition(row[1]['element'])
        Number=len(comp) 
        Number_of_Elements.append(Number)   
    TargetElementDataFrame['Number_Elements']= Number_of_Elements 

    #  Sum_of_Subscript
    Sum_Subscript_List=[]
    for row in TargetElementDataFrame.iterrows():   
        comp = Composition(row[1]['element'])
        Sum_Subscript_atoms =comp.num_atoms
        Sum_Subscript_List.append(Sum_Subscript_atoms)
    TargetElementDataFrame['Sum_Subscript']= Sum_Subscript_List

    total_pettifor_number_Based_Elemental_range=[]
    total_pettifor_number_Based_fraction_range=[] 

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)  
        pettifor_number_List=[]  
        pettifor_number_List_Based_on_fraction=[] 
        fraction_List=[]                       

        for k, v in Element_dict.items(): # k=key and v= value
            elem_str=element(str(k))
            elem_pettifor_number=elem_str.pettifor_number 

            if elem_pettifor_number==None:
                elem_pettifor_number=120

            pettifor_number_List.append(elem_pettifor_number)       
            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)
            
            pettifor_number_Based_on_fraction=elem_pettifor_number*fraction_Elements 
            pettifor_number_List_Based_on_fraction.append(pettifor_number_Based_on_fraction)

        total_pettifor_number_Based_Elemental_range.append(np.ptp(pettifor_number_List))
        total_pettifor_number_Based_fraction_range.append(np.ptp(pettifor_number_List_Based_on_fraction))

    TargetElementDataFrame['range_Pettifor_Elemental']=total_pettifor_number_Based_Elemental_range
    TargetElementDataFrame['range_Pettifor_fraction']=total_pettifor_number_Based_fraction_range

    # Electrical_conductivity= Electric_Conduct 
    Create_Features_dict_Electric_Cond = {'element':['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al','Si','P',
    'S','Cl','Ar','K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y',
    'Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu',
    'Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn','Fr',
    'Ra','Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Lr','Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg',
    'Cn','Nh','Fl','Mc','Lv','Ts','Og'],

    'Elec_Conductivity_MS_m':['0.00012','0.00012','11','25','0.000000001','0.1','0.00012','0.00012','0.00012',
    '0.0001','21','23','38','0.001','10','0.000000000000000000001','0.00000001','0.00012','14','29','1.8','2.5','5','7.9','0.62',
    '10','17','14','59','17','7.1','0.002','3.3','0.00012','0.00000000000000001','0.00012','8.3','7.7','1.8','2.4',
    '6.7','20','5','14','23','10','62','14','12','9.1','2.5','0.01','0.0000000000001','0.00012',
    '5','2.9','1.6','1.4','1.4','1.6','1.3','1.1','1.1','0.77','0.83','1.1','1.1','1.2','1.4','3.6',
    '1.8','3.3','7.7','20','5.6','12','21','9.4','45','1.0','6.7','4.8','0.77','2.3','0.00012'
    ,'0.00012','0.00012','1.0','0.00012','6.7','5.6','3.6','0.83','0.67','0.00012','0.00012','0.00012','0.00012','0.00012',
    '0.00012','0.00012','0.00012','0.00012','0.00012','0.00012','0.00012','0.00012','0.00012','0.00012','0.00012','0.00012',
    '0.00012','0.00012','0.00012','0.00012','0.00012','0.00012','0.00012']}                                
    Electrical_Conductivity = pd.TargetElementDataFrame(Create_Features_dict_Electric_Cond) 

    total_Electric_Conduct_Based_Subscript_mean=[]
    total_Electric_Conduct_Based_fraction_min=[]
    total_Electric_Conduct_Based_Subscript_variance=[]
    total_Electric_Conduct_Based_fraction_range=[]
    total_Electric_Conduct_Based_Elemental_mean=[]
    total_Electric_Conduct_Based_Subscript_range=[]  

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)

        Electric_Conduct_List=[]  
        Electric_Conduct_List_Based_on_fraction=[] 
        fraction_List=[]                       
        elem_Electric_Conduct_Subscript_List=[]  

        for k, v in Element_dict.items(): # k=key and v= value
            Electrical_Conductiv=Electrical_Conductivity[Electrical_Conductivity['element']==str(k)]['Elec_Conductivity_MS_m'].values[0]
            Electrical_Conductiv=float(Electrical_Conductiv)

            if Electrical_Conductiv==None:
                Electrical_Conductiv=1

            Electric_Conduct_List.append(Electrical_Conductiv)       
            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)
            
            Electric_Conduct_Based_on_fraction=Electrical_Conductiv*fraction_Elements 
            Electric_Conduct_List_Based_on_fraction.append(Electric_Conduct_Based_on_fraction)

            elem_Electric_Conduct_Subscript=(Electrical_Conductiv*v)
            elem_Electric_Conduct_Subscript_List.append(elem_Electric_Conduct_Subscript)

        total_Electric_Conduct_Based_Subscript_mean.append(np.mean(elem_Electric_Conduct_Subscript_List))
        total_Electric_Conduct_Based_fraction_min.append(np.min(Electric_Conduct_List_Based_on_fraction))
        total_Electric_Conduct_Based_Subscript_variance.append(np.var(elem_Electric_Conduct_Subscript_List))
        total_Electric_Conduct_Based_fraction_range.append(np.ptp(Electric_Conduct_List_Based_on_fraction))
        total_Electric_Conduct_Based_Elemental_mean.append(np.mean(Electric_Conduct_List))
        total_Electric_Conduct_Based_Subscript_range.append(np.ptp(elem_Electric_Conduct_Subscript_List))

    TargetElementDataFrame['mean_Electric_Conduct_Subscript']=total_Electric_Conduct_Based_Subscript_mean
    TargetElementDataFrame['min_Electric_Conduct_fraction']=total_Electric_Conduct_Based_fraction_min
    TargetElementDataFrame['variance_Electric_Conduct_Subscript']=total_Electric_Conduct_Based_Subscript_variance
    TargetElementDataFrame['range_Electric_Conduct_fraction']=total_Electric_Conduct_Based_fraction_range
    TargetElementDataFrame['mean_Electric_Conduct_Elemental']=total_Electric_Conduct_Based_Elemental_mean
    TargetElementDataFrame['range_Electric_Conduct_Subscript']=total_Electric_Conduct_Based_Subscript_range


    # thermal_conductivity= Thermal_conduct
    Create_Features_dict_Thermal_Comd = {'element':['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al','Si','P',
    'S','Cl','Ar','K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y',
    'Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu',
    'Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn','Fr',
    'Ra','Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Lr','Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg',
    'Cn','Nh','Fl','Mc','Lv','Ts','Og'],

    'Thermal_Conductivity_W_mk':['0.1805','0.1513','85','190','27','140','0.02583','0.02658','0.0277',
    '0.0491','140','160','235','150','0.236','0.205','0.0089','0.01772','100','200','16','22','31','94','7.8',
    '80','100','91','400','120','29','60','50','0.52','0.12','0.00943','58','35','17','23',
    '54','139','51','120','150','72','430','97','82','67','24','3','0.449','0.00565',
    '36','18','13','11','13','17','15','13','14','11','11','11','16','15','17','39',
    '16','23','57','170','48','88','150','72','320','8.3','46','35','8','20','2'
    ,'0.00361','15','19','12','54','47','27','6','6','10','10','10','10','10',
    '10','10','10','10','23','58','19','0.01','0.01','0.01','0.01','0.01','0.01','0.01','0.01','0.01','0.01','0.01','0.0023']}                               
    Thermal_Conductivity = pd.TargetElementDataFrame(Create_Features_dict_Thermal_Comd)

    total_Thermal_conduct_Based_Subscript_range=[]
    total_Thermal_conduct_Based_fraction_median=[]
    total_Thermal_conduct_Based_Elemental_range=[]
    total_Thermal_conduct_Based_fraction_min=[]
    total_Thermal_conduct_Based_Subscript_min=[]

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)

        Thermal_conduct_List=[]  
        Thermal_conduct_List_Based_on_fraction=[] 
        fraction_List=[]                       
        elem_Thermal_conduct_Subscript_List=[]  

        for k, v in Element_dict.items(): # k=key and v= value
            Thermal_Conductiv=Thermal_Conductivity[Thermal_Conductivity['element']==str(k)]['Thermal_Conductivity_W_mk'].values[0]
            Thermal_Conductiv=float(Thermal_Conductiv)
            
            Thermal_conduct_List.append(Thermal_Conductiv)       
            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)
            
            Thermal_conduct_Based_on_fraction=Thermal_Conductiv*fraction_Elements 
            Thermal_conduct_List_Based_on_fraction.append(Thermal_conduct_Based_on_fraction)

            elem_Thermal_conduct_Subscript=(Thermal_Conductiv*v)
            elem_Thermal_conduct_Subscript_List.append(elem_Thermal_conduct_Subscript)

        total_Thermal_conduct_Based_Subscript_range.append(np.ptp(elem_Thermal_conduct_Subscript_List))
        total_Thermal_conduct_Based_fraction_median.append(np.median(Thermal_conduct_List_Based_on_fraction))
        total_Thermal_conduct_Based_Elemental_range.append(np.ptp(Thermal_conduct_List))
        total_Thermal_conduct_Based_fraction_min.append(np.min(Thermal_conduct_List_Based_on_fraction))
        total_Thermal_conduct_Based_Subscript_min.append(np.min(elem_Thermal_conduct_Subscript_List))

    TargetElementDataFrame['range_Thermal_conduct_Subscript']=total_Thermal_conduct_Based_Subscript_range
    TargetElementDataFrame['median_Thermal_conduct_fraction']=total_Thermal_conduct_Based_fraction_median
    TargetElementDataFrame['range_Thermal_conduct_Elemental']=total_Thermal_conduct_Based_Elemental_range
    TargetElementDataFrame['min_Thermal_conduct_fraction']=total_Thermal_conduct_Based_fraction_min
    TargetElementDataFrame['min_Thermal_conduct_Subscript']=total_Thermal_conduct_Based_Subscript_min

    # heat_of_formation=heat_formation
    total_heat_formation_Based_fraction_mean=[]
    total_heat_formation_Based_Elemental_Ave_dev =[]   # Ave_dev=Average_deviation 
    total_heat_formation_Based_fraction_max=[]
    total_heat_formation_Based_Subscript_median=[]

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"])
        Elem = row["element"]
        comp = Composition(Elem) 

        heat_formation_List=[]  
        heat_formation_List_Based_on_fraction=[] 
        fraction_List=[]                       
        elem_heat_formation_Subscript_List=[] 

        for k, v in Element_dict.items(): # k=key and v= value
            elem_str=element(str(k))
            elem_heat_formation=elem_str.heat_of_formation 

            if elem_heat_formation==None:
                elem_heat_formation=170
        
            heat_formation_List.append(elem_heat_formation)       
            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)
            
            heat_formation_Based_on_fraction=elem_heat_formation*fraction_Elements 
            heat_formation_List_Based_on_fraction.append(heat_formation_Based_on_fraction)

            elem_heat_formation_Subscript=(elem_heat_formation*v)
            elem_heat_formation_Subscript_List.append(elem_heat_formation_Subscript)

        # Average_deviation calculate
        mean_value = np.mean(heat_formation_List)
        res=[]
        for ele in heat_formation_List:
            res.append(abs(ele - mean_value))   
        total_heat_formation_Based_Elemental_Ave_dev.append(np.mean(res))
        total_heat_formation_Based_fraction_mean.append(np.mean(heat_formation_List_Based_on_fraction))
        total_heat_formation_Based_fraction_max.append(np.max(heat_formation_List_Based_on_fraction))
        total_heat_formation_Based_Subscript_median.append(np.median(elem_heat_formation_Subscript_List))

    TargetElementDataFrame['mean_heat_formation_fraction']=total_heat_formation_Based_fraction_mean
    TargetElementDataFrame['Ave_dev_heat_formation_Elemental']=total_heat_formation_Based_Elemental_Ave_dev
    TargetElementDataFrame['max_heat_formation_fraction']=total_heat_formation_Based_fraction_max
    TargetElementDataFrame['median_heat_formation_Subscript']=total_heat_formation_Based_Subscript_median


    # Ionic_Radius
    Ionic_Radius_Element_periodic_tabel = {'element':['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al','Si','P',
    'S','Cl','Ar','K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y',
    'Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu',
    'Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn','Fr',
    'Ra','Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Lr','Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg',
    'Cn','Nh','Fl','Mc','Lv','Ts','Og'],

    'Ionic_Radius_Based_Angestrom':['1.48','0.4','0.76','0.27','0.11','0.15','1.46','1.38','1.3',
    '0.4','1.02','0.72','0.39','0.26','0.17','1.84','1.81','0.4','1.38','1.12','0.745','0.67','0.46','0.615','0.83',
    '0.78','0.745','0.69','0.65','0.6','0.47','0.39','0.335','1.98','1.96','0.4','1.56','1.21','0.9','0.72',
    '0.72','0.65','0.645','0.68','0.665','0.86','0.94','0.87','0.8','0.69','0.76','2.21','2.2','0.4',
    '1.74','1.35','1.1','1.01','0.99','1.109','0.97','1.079','1.01','1','0.923','0.912','0.901','0.89','0.88','0.868'
    ,'0.861','0.83','0.69','0.62','0.63','0.49','0.625','0.625','0.85','0.69','1.5','1.19','1.03','0.94',
    '0.62','0.4','1.8','1.7','1.12','1.09','1.01','1','0.98','0.96','1.09','0.95','0.93','0.92','0.4','0.4','0.4','1.1','0.4',
    '0.4','0.4','0.4','0.4','0.4','0.4','0.4','0.4','0.4','0.4','0.4','0.4','0.4','0.4','0.4']}                               
    df_Ionic_Radius_Element_periodic_tabel = pd.TargetElementDataFrame(Ionic_Radius_Element_periodic_tabel)

    total_Ionic_Radius_Based_Elemental_median=[]
    total_Ionic_Radius_Based_Elemental_max=[]

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)

        Ionic_Radius_List=[]  

        for k, v in Element_dict.items(): # k=key and v= value
            Ionic_Radius=df_Ionic_Radius_Element_periodic_tabel[df_Ionic_Radius_Element_periodic_tabel['element']==str(k)]['Ionic_Radius_Based_Angestrom'].values[0]
            Ionic_Radius=float(Ionic_Radius)

            if Ionic_Radius==None:
                Ionic_Radius=1

            Ionic_Radius_List.append(Ionic_Radius)    


        total_Ionic_Radius_Based_Elemental_median.append(np.median(Ionic_Radius_List))
        total_Ionic_Radius_Based_Elemental_max.append(np.max(Ionic_Radius_List))

    TargetElementDataFrame['median_Ionic_Radius_Elemental']=total_Ionic_Radius_Based_Elemental_median
    TargetElementDataFrame['max_Ionic_Radius_Elemental']=total_Ionic_Radius_Based_Elemental_max

    #ElecAffinity
    Create_Features_dict_wiki = {'element':['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al','Si','P',
    'S','Cl','Ar','K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y',
    'Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu',
    'Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn','Fr',
    'Ra','Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Lr','Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg',
    'Cn','Nh','Fl','Mc','Lv','Ts','Og'],

    'Electron_affinity_Wiki_Kj_mol':['72.76','-48','59.63','-48','26.98','121.77','-6.8','140.97','328.16',
    '-116','52.86','-40','41.76','134.06','72.03','200.41','348.57','-96','48.38','2.37','18','7.28','50.91','65.21','-50',
    '14.78','63.89','111.65','119.23','-58','29.06','118.93','77.65','194.95','324.53','-96','46.88','5.02','29.6','41.80',
    '88.51','72.1','53','100.96','110.27','54.24','125.86','-68','37.04','107.29','101.05','190.16','295.15','-77',
    '45.5','13.95','53.79','55','10.53','9.4','12.45','15.63','11.2','13.22','12.67','33.96','32.61','30.1','99','-1.93',
    '23.04','17.18','31','78.76','5.82','103.99','150.90','205.04','222.74','-48','30.88','34.41','90.92','136','233.08'
    ,'-68','46.89','9.64','33.77','58.63','53.03','30.39','45.85','-48.33','9.93','27.17','-165.24','-97.31','-28.60',
    '33.96','93.91','-223.22','-30.04','0','0','0','0','0','0','0','151','0','66.6','0','35.3','74.9','165.9','5.4']}                               
    ElecAffinity_Wiki = pd.TargetElementDataFrame(Create_Features_dict_wiki)  

    total_ElecAffinity_Based_fraction_max=[]
    total_ElecAffinity_Based_Subscript_min=[]
    total_ElecAffinity_Based_Elemental_mean=[]
    total_ElecAffinity_Based_Elemental_range=[]
    total_ElecAffinity_Based_Elemental_min=[] 
    total_ElecAffinity_Based_fraction_median=[] 

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem) 

        ElecAffinity_List=[]  
        ElecAffinity_List_Based_on_fraction=[] 
        fraction_List=[]                        
        elem_ElecAffinity_Subscript_List=[]  

        for k, v in Element_dict.items(): # k=key and v= value
            # print(k)
            ElecAffinity=ElecAffinity_Wiki[ElecAffinity_Wiki['element']==str(k)]['Electron_affinity_Wiki_Kj_mol'].values[0]
            ElecAffinity=float(ElecAffinity)

            ElecAffinity_List.append(ElecAffinity)       
            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)

            ElecAffinity_Based_on_fraction=ElecAffinity*fraction_Elements 
            ElecAffinity_List_Based_on_fraction.append(ElecAffinity_Based_on_fraction)

            elem_ElecAffinity_Subscript=(ElecAffinity*v)
            elem_ElecAffinity_Subscript_List.append(elem_ElecAffinity_Subscript)
            
        total_ElecAffinity_Based_fraction_max.append(np.max(ElecAffinity_List_Based_on_fraction))
        total_ElecAffinity_Based_Subscript_min.append(np.min(elem_ElecAffinity_Subscript_List))
        total_ElecAffinity_Based_Elemental_mean.append(np.mean(ElecAffinity_List))
        total_ElecAffinity_Based_Elemental_range.append(np.ptp(ElecAffinity_List))
        total_ElecAffinity_Based_Elemental_min.append(np.min(ElecAffinity_List))

    TargetElementDataFrame['max_ElecAffinity_fraction']=total_ElecAffinity_Based_fraction_max
    TargetElementDataFrame['min_ElecAffinity_Subscript']=total_ElecAffinity_Based_Subscript_min
    TargetElementDataFrame['mean_ElecAffinity_Elemental']=total_ElecAffinity_Based_Elemental_mean
    TargetElementDataFrame['range_ElecAffinity_Elemental']=total_ElecAffinity_Based_Elemental_range
    TargetElementDataFrame['min_ElecAffinity_Elemental']=total_ElecAffinity_Based_Elemental_min

    #  dipole_polarizability
    total_dipole_polarizability_Based_Subscript_range=[]
    total_dipole_polarizability_Based_Elemental_min=[]
    total_dipole_polarizability_Based_Elemental_range=[]
    total_dipole_polarizability_Based_fraction_min=[] 
    total_dipole_polarizability_Based_fraction_range=[]
    total_dipole_polarizability_Based_Elemental_median=[] 

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)

        dipole_polarizability_List=[]  
        dipole_polarizability_List_Based_on_fraction=[] 
        fraction_List=[]                       
        elem_dipole_polarizability_Subscript_List=[]  

        for k, v in Element_dict.items(): # k=key and v= value
            elem_str=element(str(k))
            elem_dipole_polarizability=elem_str.dipole_polarizability

            if elem_dipole_polarizability==None:
                elem_dipole_polarizability=75

            dipole_polarizability_List.append(elem_dipole_polarizability)       
            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)
            
            dipole_polarizability_Based_on_fraction=elem_dipole_polarizability*fraction_Elements 
            dipole_polarizability_List_Based_on_fraction.append(dipole_polarizability_Based_on_fraction)

            elem_dipole_polarizability_Subscript=(elem_dipole_polarizability*v)
            elem_dipole_polarizability_Subscript_List.append(elem_dipole_polarizability_Subscript)

        total_dipole_polarizability_Based_Subscript_range.append(np.ptp(elem_dipole_polarizability_Subscript_List))
        total_dipole_polarizability_Based_Elemental_min.append(np.min(dipole_polarizability_List))
        total_dipole_polarizability_Based_Elemental_range.append(np.ptp(dipole_polarizability_List))
        total_dipole_polarizability_Based_fraction_min.append(np.min(dipole_polarizability_List_Based_on_fraction))
        total_dipole_polarizability_Based_fraction_range.append(np.ptp(dipole_polarizability_List_Based_on_fraction))
        total_dipole_polarizability_Based_Elemental_median.append(np.median(dipole_polarizability_List))
        total_ElecAffinity_Based_fraction_median.append(np.median(ElecAffinity_List_Based_on_fraction))

    TargetElementDataFrame['range_dipole_polarizability_Subscript']=total_dipole_polarizability_Based_Subscript_range
    TargetElementDataFrame['min_dipole_polarizability_Elemental']=total_dipole_polarizability_Based_Elemental_min
    TargetElementDataFrame['range_dipole_polarizability_Elemental']=total_dipole_polarizability_Based_Elemental_range
    TargetElementDataFrame['min_dipole_polarizability_fraction']=total_dipole_polarizability_Based_fraction_min
    TargetElementDataFrame['range_dipole_polarizability_fraction']=total_dipole_polarizability_Based_fraction_range
    TargetElementDataFrame['median_dipole_polarizability_Elemental']=total_dipole_polarizability_Based_Elemental_median
    TargetElementDataFrame['median_ElecAffinity_fraction']=total_ElecAffinity_Based_fraction_median


    # Specific heat= Specific_heat 
    Create_Features_dict_Specific_heat = {'element':['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al','Si','P',
    'S','Cl','Ar','K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y',
    'Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu',
    'Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At','Rn','Fr',
    'Ra','Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf','Es','Fm','Md','No','Lr','Rf','Db','Sg','Bh','Hs','Mt','Ds','Rg',
    'Cn','Nh','Fl','Mc','Lv','Ts','Og'],

    'Specific_heat_J_KgK':['14300','5193.1','3570','1820','1030','710','1040','919','824',
    '1030','1230','1020','904','710','769.7','705','478.2','520.33','757','631','567','520','489','448','479',
    '449','421','445','384.4','388','371','321.4','328','321.2','947.3','248.05','364','300','298','278',
    '265','251','63','238','240','240','235','230','233','217','207','201','429','158.32',
    '242','205','195','192','193','190','180','196','182','240','182','167','165','168','160','154',
    '154','144','140','132','137','130','131','133','129.1','139.5','129','127','122','100',
    '90','93.65','90','92','120','118','99.1','116',
    '100','100','100','100','100','100','100','100','100','100','100','100','100','100','100'
    ,'100','100','100','100','100','100','100','100','100','100','100']}                               
    Specific_heat = pd.TargetElementDataFrame(Create_Features_dict_Specific_heat)     

    total_Specific_heat_Based_fraction_min=[]
    total_Specific_heat_Based_Elemental_min=[]
    total_Specific_heat_Based_Elemental_max=[]
    total_Specific_heat_Based_Subscript_variance=[]

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)

        Specific_heat_List=[]  
        Specific_heat_List_Based_on_fraction=[] 
        fraction_List=[]                       
        elem_Specific_heat_Subscript_List=[]  

        for k, v in Element_dict.items(): # k=key and v= value
            Specific_heat_1=Specific_heat[Specific_heat['element']==str(k)]['Specific_heat_J_KgK'].values[0]
            Specific_heat_1=float(Specific_heat_1)

            if Specific_heat_1==None:
                Specific_heat_1=200

            Specific_heat_List.append(Specific_heat_1)       
            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)
            
            Specific_heat_Based_on_fraction=Specific_heat_1*fraction_Elements 
            Specific_heat_List_Based_on_fraction.append(Specific_heat_Based_on_fraction)

            elem_Specific_heat_Subscript=(Specific_heat_1*v)
            elem_Specific_heat_Subscript_List.append(elem_Specific_heat_Subscript)

        total_Specific_heat_Based_fraction_min.append(np.min(Specific_heat_List_Based_on_fraction))
        total_Specific_heat_Based_Elemental_min.append(np.min(Specific_heat_List))
        total_Specific_heat_Based_Elemental_max.append(np.max(Specific_heat_List))
        total_Specific_heat_Based_Subscript_variance.append(np.var(elem_Specific_heat_Subscript_List))

    TargetElementDataFrame['min_Specific_heat_fraction']=total_Specific_heat_Based_fraction_min
    TargetElementDataFrame['min_Specific_heat_Elemental']=total_Specific_heat_Based_Elemental_min
    TargetElementDataFrame['max_Specific_heat_Elemental']=total_Specific_heat_Based_Elemental_max
    TargetElementDataFrame['variance_Specific_heat_Subscript']=total_Specific_heat_Based_Subscript_variance

    #  First Ionisation Energy= First_Ionis_Energy
    total_First_Ionis_Energy_Based_Elemental_mean=[]

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)

        First_Ionis_Energy_List=[]                    

        for k, v in Element_dict.items(): # k=key and v= value
            elem_str=element(str(k))
            Atomic_number_element=elem_str.atomic_number
            elem_First_Ionis_Energy =fetch_ionization_energies(degree=1)['IE1'][Atomic_number_element]


            if elem_First_Ionis_Energy==None:
                elem_First_Ionis_Energy=1

            First_Ionis_Energy_List.append(elem_First_Ionis_Energy)       

        total_First_Ionis_Energy_Based_Elemental_mean.append(np.mean(First_Ionis_Energy_List))   

    TargetElementDataFrame['mean_First_Ionis_Energy_Elemental']=total_First_Ionis_Energy_Based_Elemental_mean


    # Electrongativity
    total_electronegativ_Based_fraction_max=[]
    total_electronegativ_Based_Elemental_range=[] 
    total_electronegativ_Based_Elemental_median=[]

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem) 

        electronegativ_List=[]
        electronegativ_List_Based_on_fraction=[] 
        fraction_List=[]                       

        for k, v in Element_dict.items(): # k=key and v= value
            elem_str=element(str(k))
            elem_electronegativ=elem_str.electronegativity('pauling')

            if elem_electronegativ==None:
                elem_electronegativ=0.1
            if k == 'Kr':
                elem_electronegativ=3

            electronegativ_List.append(elem_electronegativ)       
            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)
            
            electronegativ_Based_on_fraction=elem_electronegativ*fraction_Elements 
            electronegativ_List_Based_on_fraction.append(electronegativ_Based_on_fraction)


        total_electronegativ_Based_Elemental_range.append(np.ptp(electronegativ_List))
        total_electronegativ_Based_fraction_max.append(np.max(electronegativ_List_Based_on_fraction))
        total_electronegativ_Based_Elemental_median.append(np.median(electronegativ_List))

    TargetElementDataFrame['max_ElecGativ_Fraction']=total_electronegativ_Based_fraction_max
    TargetElementDataFrame['range_ElecGativ_Elemental']=total_electronegativ_Based_Elemental_range
    TargetElementDataFrame['median_ElecGativ_Elemental']=total_electronegativ_Based_Elemental_median

    # unpaired_electrons
    total_unpaired_electron_Based_Elemental_min=[]
    total_unpaired_electron_Based_fraction_range=[] 
    total_unpaired_electron_Based_Subscript_range=[] 

    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)  

        unpaired_electron_List=[] 
        unpaired_electron_List_Based_on_fraction=[] 
        fraction_List=[]                     
        elem_unpaired_electron_Subscript_List=[]  

        for k, v in Element_dict.items(): # k=key and v= value
            elem_str=element(str(k))
            elem_unpaired_electron=elem_str.ec.unpaired_electrons() 

            if elem_unpaired_electron==None:
                elem_unpaired_electron=1

            unpaired_electron_List.append(elem_unpaired_electron)       
            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)
            
            unpaired_electron_Based_on_fraction=elem_unpaired_electron*fraction_Elements 
            unpaired_electron_List_Based_on_fraction.append(unpaired_electron_Based_on_fraction)

            elem_unpaired_electron_Subscript=(elem_unpaired_electron*v)
            elem_unpaired_electron_Subscript_List.append(elem_unpaired_electron_Subscript)

        total_unpaired_electron_Based_Elemental_min.append(np.min(unpaired_electron_List))
        total_unpaired_electron_Based_fraction_range.append(np.ptp(unpaired_electron_List_Based_on_fraction))
        total_unpaired_electron_Based_Subscript_range.append(np.ptp(elem_unpaired_electron_Subscript_List))

    TargetElementDataFrame['min_UnpairedElec_Elemental']=total_unpaired_electron_Based_Elemental_min
    TargetElementDataFrame['range_UnpairedElec_fraction']=total_unpaired_electron_Based_fraction_range
    TargetElementDataFrame['range_UnpairedElec_Subscript']=total_unpaired_electron_Based_Subscript_range

    # period number
    total_period_number_Based_Elemental_mean=[]
    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"])
        Elem = row["element"]
        comp = Composition(Elem)

        period_number_List=[]  
        period_number_List_Based_on_fraction=[] 
        fraction_List=[]                        
        elem_period_number_Subscript_List=[] 

        for k, v in Element_dict.items(): # k=key and v= value
            elem_str=element(str(k))
            elem_period_number=elem_str.period

            if elem_period_number==None:
                elem_period_number=3

            period_number_List.append(elem_period_number) 
        total_period_number_Based_Elemental_mean.append(np.mean(period_number_List))

    TargetElementDataFrame['mean_period_Elemental']=total_period_number_Based_Elemental_mean

    #  number electron valence= Number_Elec_Valence
    total_Number_Elec_Valence_Based_Elemental_min=[]
    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)
        Number_Elec_Valence_List=[]                       

        for k, v in Element_dict.items(): # k=key and v= value
            elem_str=element(str(k))
            elem_Number_Elec_Valence=elem_str.nvalence()

            if elem_Number_Elec_Valence==None:
                elem_Number_Elec_Valence=2

            Number_Elec_Valence_List.append(elem_Number_Elec_Valence)

        total_Number_Elec_Valence_Based_Elemental_min.append(np.min(Number_Elec_Valence_List))

    TargetElementDataFrame['min_NumElecValence_Elemental']=total_Number_Elec_Valence_Based_Elemental_min

    # spin_only_magnetic_moment=MagMoment
    total_MagMoment_Based_fraction_median=[]
    for index, row in TargetElementDataFrame.iterrows():
        Element_dict=chemparse.parse_formula(row["element"]) 
        Elem = row["element"]
        comp = Composition(Elem)
        MagMoment_List_Based_on_fraction=[] 
        fraction_List=[]                       

        for k, v in Element_dict.items(): # k=key and v= value
            elem_str=element(str(k))
            elem_MagMoment=elem_str.nvalence()

            if elem_MagMoment==None:
                elem_MagMoment=2

            fraction_Elements = comp.get_atomic_fraction(k)
            fraction_List.append(fraction_Elements)       
            MagMoment_Based_on_fraction=elem_MagMoment*fraction_Elements 
            MagMoment_List_Based_on_fraction.append(MagMoment_Based_on_fraction)

        total_MagMoment_Based_fraction_median.append(np.median(MagMoment_List_Based_on_fraction))

    TargetElementDataFrame['median_MagMoment_fraction']=total_MagMoment_Based_fraction_median

    TargetElementDataFrame.index=range(0,len(TargetElementDataFrame))
    # ele_fra_mat=element_fraction_material
    ele_fra_mat=np.zeros([len(TargetElementDataFrame), 103])
    ef = ElementFraction()
    for index in TargetElementDataFrame.index:
        # print(DataG_1.loc[index]["element"])
        com=Composition(TargetElementDataFrame.loc[index]["element"])
        ele_fra_mat[index] = ef.featurize(com)

    df_groupby_Fraction_Element=pd.TargetElementDataFrame(ele_fra_mat,columns=['H_Frac','He_Frac','Li_Frac','Be_Frac','B_Frac','C_Frac','N_Frac','O_Frac','F_Frac',
    'Ne_Frac','Na_Frac','Mg_Frac','Al_Frac','Si_Frac','P_Frac',
    'S_Frac','Cl_Frac','Ar_Frac','K_Frac','Ca_Frac','Sc_Frac','Ti_Frac','V_Frac','Cr_Frac','Mn_Frac','Fe_Frac','Co_Frac','Ni_Frac',
    'Cu_Frac','Zn_Frac','Ga_Frac','Ge_Frac','As_Frac','Se_Frac','Br_Frac','Kr_Frac','Rb_Frac','Sr_Frac','Y_Frac',
    'Zr_Frac','Nb_Frac','Mo_Frac','Tc_Frac','Ru_Frac','Rh_Frac','Pd_Frac','Ag_Frac','Cd_Frac','In_Frac','Sn_Frac','Sb_Frac',
    'Te_Frac','I_Frac','Xe_Frac','Cs_Frac','Ba_Frac','La_Frac','Ce_Frac','Pr_Frac','Nd_Frac','Pm_Frac','Sm_Frac','Eu_Frac',
    'Gd_Frac','Tb_Frac','Dy_Frac','Ho_Frac','Er_Frac','Tm_Frac','Yb_Frac','Lu_Frac','Hf_Frac','Ta_Frac','W_Frac','Re_Frac',
    'Os_Frac','Ir_Frac','Pt_Frac','Au_Frac','Hg_Frac','Tl_Frac','Pb_Frac','Bi_Frac','Po_Frac','At_Frac','Rn_Frac','Fr_Frac',
    'Ra_Frac','Ac_Frac','Th_Frac','Pa_Frac','U_Frac','Np_Frac','Pu_Frac','Am_Frac','Cm_Frac','Bk_Frac','Cf_Frac',
    'Es_Frac','Fm_Frac','Md_Frac','No_Frac','Lr_Frac'])
    # df_groupby_Fraction_Element
    TargetElementDataFrame= pd.concat([TargetElementDataFrame, df_groupby_Fraction_Element], axis=1, join='inner')
    New_Features_Test=TargetElementDataFrame[List_Features_47_103]
    model_total_saved=CatBoostRegressor()
    model_total_saved.load_model('./model_total')
    predictions = model_total_saved.predict(New_Features_Test)
    # predictions = model_total.predict(New_Features_Test)
    print(predictions[0],end=" ")
    return predictions[0]
                    



